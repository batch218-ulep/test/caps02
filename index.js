const express = require('express')
const cors = require('cors')
const colors = require('colors')
const mongoose = require("mongoose");

mongoose.connect("mongodb+srv://root:root123@ulep-capstone2.fgrdyue.mongodb.net/?retryWrites=true&w=majority", {
	useNewUrlParser:true, 
	useUnifiedTopology:true
});

mongoose.connection.once("open", () => 
	console.log(`Now connected to Ulep: E-COMMERCE-API`));

const app = express()

/* Middleware */
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

/* Routes */
app.use('/users', require('./routes/users.js'))
app.use('/products', require('./routes/product.js'))
app.use('/orders', require('./routes/order.js'))
app.use('/carts', require('./routes/cart.js'))
const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`API is now online at Port: ${port}`))

