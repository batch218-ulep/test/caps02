const mongoose = require('mongoose')

const queriesSchema = new mongoose.Schema({
  firstName: {
    type: String,
    trim: true,
    required: [true, "Firstname is required!"]
  },

  lastName: {
    type: String,
    trim: true,
    required: [true, "Lastname is required!"]
  },

  email: {
    type: String,
    trim: true,
    required: [true, "Email is required!"]
  },

  address: {
    type: String,
    trim: true,
    required: [true, "Address is required"]
  },

  mobileNo: {
    type: String,
    trim: true,
    required: [true, "Mobile number is required!"]
  },

  source: {
    type: String,
    trim: true,
    default: 'na'
  },

  isAdmin: {
    type: Boolean,
    default: false
  },

  createdOn: {
    type: Date,
    default: new Date()
  }
})

module.exports = mongoose.model('Queries', userSchema)