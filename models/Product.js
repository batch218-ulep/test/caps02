const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: [true, 'Product name is required!']
  },

  description: {
    type: String,
    trim: true,
    required: [true, 'Product description is required!']
  },

  categories: {
    type: Array,
    required: [true, 'description is required']
  },

  brand: {
    type: String,
    trim: true,
    required: [true, 'Brand is required!']
  },


  screenSize: {
    type: String,
    trim: true,
    required: [true, 'screen is required!']
  },

  cpu: {
    type: String,
    trim: true,
    required: [true, 'cpu is required!']
  },

  videoCard: {
    type: String,
    trim: true,
    required: [true, 'video card is required!']
  },

  ram: {
    type: String,
    trim: true,
    required: [true, 'ram is required']
  },

  storage: {
    type: String,
    trim: true,
    required: [true, 'starage is required'],
  },

  OS: {
    type: String,
    trim: true,
    required: [true, 'Operating ssytem is required!']
  },

  source: {
    type: String,
    default: 'no image path'
  },

  price: {
    type: Number,
    required: [true, 'Price is required!']
  },

  isActive: {
    type: Boolean,
    default: true
  },

  createdOn: {
    type: Date,
    default: new Date()
  }

})

module.exports = mongoose.model('Product', productSchema)