
const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "User Id is required"]
  },

  firstName: {
    type: String,
    required: [true, 'Firstname is required!']
  },

  lastName: {
    type: String,
    required: [true, 'Lastname is required!']
  },


  products: [{
    productId: {
      type: String,
      required: [true, "Product Id is required"]
    },

    productCategories: {
      type: Array,
      required: [true, "Product category is required"]
    },

    productBrand: {
      type: String,
      required: [true, "Product brand is required"]
    },

    productName: {
      type: String,
      required: [true, "Product name is required"]
    },

    productScreenSize: {
      type: String,
      required: [true, "Product screen size is required"]
    },

    productCPU: {
      type: String,
      required: [true, "Product cpu size is required"]
    },

    productVideoCard: {
      type: String,
      required: [true, "Product video card size is required"]
    },

    productRam: {
      type: String,
      required: [true, "Product RAM size is required"]
    },

    productStorage: {
      type: String,
      required: [true, "Product storage size is required"]
    },

    productOS: {
      type: String,
      required: [true, "Product operating System size is required"]
    },

    productPrice: {
      type: Number,
      required: [true, "Product price is required"]
    },

    quantity: {
      type: Number,
      default: 1
    },

    totalAmount: {
      type: Number,
      default: 0
    },

    purchased: {
      type: Date,
      default: new Date
    }
  }],

  isActive: {
    type: Boolean,
    default: false
  },

  Subtotal: {
    type: Number,
    default: 0
  },

  purchased: {
    type: Date,
    default: new Date
  }
})

module.exports = mongoose.model('Cart', cartSchema)