const express = require('express')
const router = express.Router()
const auth = require('../auth.js')
const { userRegister, userLogin, userProfile, readAllUser, checkSpecificProfile, userOrder, updateUserProfile, updateUserPassword, editUserData, setUserToAdmin, archiveUser, requestPassword } = require('../controller/userController.js')

// Register User
router.post('/register', (req, res) => {
    userRegister(req.body).then(
        resultFromController => res.send(resultFromController))
})

// Login user validation
router.post('/login', (req, res) => {
    userLogin(req.body).then(
        resultFromController => res.send(resultFromController))
})

//forgot Password
router.patch('/requestPassword', (req, res) => {
    requestPassword(req.body).then(
        resultFromController => res.send(resultFromController))
})

// CheckUserProfile
router.get('/profile/:userId', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    checkSpecificProfile(data, req.params.userId).then(
        resultFromController => res.send(resultFromController))
})

// Read my profile
router.get('/me', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id,
    }

    userProfile(data).then(
        resultFromController => res.send(resultFromController))
})

// Check User Order 
router.get('/order', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id,
    }

    userOrder(data).then(
        resultFromController => res.send(resultFromController))
})


// Read all user profile
router.get('/all', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    }

    readAllUser(data).then(
        resultFromController => res.send(resultFromController))
})

// Update my profile
router.patch('/update/me', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id,
        reqBody: req.body,

    }

    updateUserProfile(data).then(
        resultFromController => res.send(resultFromController))
})

// Update password 
router.patch('/update/password', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id,
        reqBody: req.body,
    }

    updateUserPassword(data).then(
        resultFromController => res.send(resultFromController))
})

// Update User Data for admin only
router.patch('/:userId/update', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        reqBody: req.body
    }

    editUserData(data, req.params.userId).then(
        resultFromController => res.send(resultFromController))
})

// Archive User
router.patch('/:userId/archive', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        reqBody: req.body
    }

    archiveUser(data, req.params.userId).then(
        resultFromController => res.send(resultFromController))
})

// Set user to became admin
router.patch('/:userId/admin', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        reqBody: req.body
    }

    setUserToAdmin(data, req.params.userId).then(
        resultFromController => res.send(resultFromController))
})




module.exports = router





