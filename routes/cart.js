const express = require('express')
const router = express.Router()
const auth = require('../auth.js')
const { addToCart, userCart, editUserCart, deleteUserCart, showAllCarts, specificCart, deleteCart } = require('../controller/cartController.js')

// Add to cart
router.post('/add', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      productId: req.body.productId,
      quantity: req.body.quantity
   }
   addToCart(data).then(
      resultFromController => res.send(resultFromController))
})

// show user product in cart
router.get('/me', auth.verify, (req, res) => {
   const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      userId: auth.decode(req.headers.authorization).id,
   }

   userCart(data).then(
      resultFromController => res.send(resultFromController))
})

// show all products in carts for admin
router.get('/all', auth.verify, (req, res) => {
   const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
   }

   showAllCarts(data).then(
      resultFromController => res.send(resultFromController))
})

// chow specific cart 

router.get('/:cartId/specificCart', auth.verify, (req, res) => {
   const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      cartId: req.params.cartId
   }

   specificCart(data).then(
      resultFromController => res.send(resultFromController))
})

// update cart order
router.patch('/:cartId/update', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      cartId: req.params.cartId,
      productId: req.body.productId,
      quantity: req.body.quantity
   }

   editUserCart(data).then(
      resultFromController => res.send(resultFromController))
})

// delete cart order
router.delete('/:cartId/delete', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      cartId: req.params.cartId,
      productId: req.body.productId,
   }

   deleteUserCart(data).then(
      resultFromController => res.send(resultFromController))
})

// delete cart order
router.delete('/delete/:cartId/all', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      cartId: req.params.cartId,
   }

   deleteCart(data).then(
      resultFromController => res.send(resultFromController))
})

module.exports = router