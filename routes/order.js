const express = require('express')
const router = express.Router()
const { createOrder, allOrder, editUserOrder, userDeleteOrder, userSpecificOrder, deleteOrder } = require('../controller/orderController.js')
const auth = require('../auth.js')


router.post('/create', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      productId: req.body.productId,
      quantity: req.body.quantity
   }

   createOrder(data).then(
      resultFromContainer => res.send(resultFromContainer))
})


// Check all Order for admin user only 
router.get('/all', auth.verify, (req, res) => {
   const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
   }

   allOrder(data).then(
      resultFromController => res.send(resultFromController))
})

// Check specific order

router.get('/:orderId/specificOrder', auth.verify, (req, res) => {
   const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      orderId: req.params.orderId
   }

   userSpecificOrder(data).then(
      resultFromController => res.send(resultFromController))
})



// Edit User Order
router.patch('/:orderId/update', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      orderId: req.params.orderId,
      productId: req.body.productId,
      quantity: req.body.quantity
   }

   editUserOrder(data).then(
      resultFromController => res.send(resultFromController))
})

// Edit User Order
router.delete('/:orderId/delete', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      orderId: req.params.orderId,
      productId: req.body.productId,
   }

   userDeleteOrder(data).then(
      resultFromController => res.send(resultFromController))
})

// delete cart order
router.delete('/delete/:orderId/all', auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      orderId: req.params.orderId,
   }

   deleteOrder(data).then(
      resultFromController => res.send(resultFromController))
})


module.exports = router
