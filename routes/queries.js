const express = require('express')
const router = express.Router()
const auth = require('../auth.js')
const { userQuery } = require('../controller/queriesController.js')



// Login user validation
router.post('/query', auth.verify, (req, res) => {

  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    userId: auth.decode(req.headers.authorization).id,
  }

  userQuery(data, reqBody).then(
    resultFromController => res.send(resultFromController))
})



module.exports = router





