const express = require('express')
const router = express.Router()
const auth = require('../auth.js')
const {
  createProduct,
  getActiveProduct,
  getSpecificProduct,
  updateProduct,
  archiveProduct,
  deleteProduct,
  allProducts,
  unArchiveProduct
} = require('../controller/productController.js')

router.post('/create', auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }
  console.log(data)
  createProduct(data).then(
    resultFromController => res.send(resultFromController))
})

router.get('/all', (req, res) => {

  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    qNew: req.query.new,
    qCategory: req.query.category
  }

  allProducts(data).then(
    resultFromController => res.send(resultFromController))
})

router.get('/active', (req, res) => {
  getActiveProduct(req.query.new, req.query.category).then(
    resultFromController => res.send(resultFromController))
})

router.get('/:productId', (req, res) => {
  getSpecificProduct(req.params.productId).then(
    resultFromController => res.send(resultFromController))
})

router.patch('/:productId/update', auth.verify, (req, res) => {
  const data = {
    productId: req.params.productId,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  updateProduct(data, req.body).then(
    resultFromController => res.send(resultFromController))
})

router.patch('/:productId/archive', auth.verify, (req, res) => {
  const data = {
    productId: req.params.productId,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  archiveProduct(data).then(
    resultFromController => res.send(resultFromController))
})

router.patch('/:productId/unarchive', auth.verify, (req, res) => {
  const data = {
    productId: req.params.productId,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  unArchiveProduct(data).then(
    resultFromController => res.send(resultFromController))
})

router.delete('/:productId/delete', auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  deleteProduct(data, req.params.productId).then(
    resultFromController => res.send(resultFromController))
})

module.exports = router