const Product = require('../models/Product.js')
const Order = require('../models/Order.js');
const User = require('../models/User.js');



const createOrder = async (data) => {
  console.log(data)
  console.log(data);
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {

    const userData = await User.findById(data.userId);
    const getProduct = await Product.findById(data.productId);


    if (!getProduct) {
      return {
        message: `Nothing found`
      }
    }

    const newOrder = new Order({
      userId: data.userId,
      firstName: userData.firstName,
      lastName: userData.lastName,
      products: [{
        productId: getProduct.id,
        productBrand: getProduct.brand,
        productCategories: getProduct.categories,
        productName: getProduct.name,
        productScreenSize: getProduct.screenSize,
        productCPU: getProduct.cpu,
        productVideoCard: getProduct.videoCard,
        productRam: getProduct.ram,
        productStorage: getProduct.storage,
        productOS: getProduct.OS,
        productPrice: getProduct.price,
        productSource: getProduct.source,
        quantity: data.quantity,
      }],
      totalAmount: data.quantity * getProduct.price
    })

    if (!Number.isInteger(data.quantity)) {
      return {
        message: `Quantity must be a number!`
      }
    }

    if (data.quantity <= 0) {
      return {
        message: `Quantity minimun value is 1`
      }
    }

    if (data.quantity > 10) {
      return {
        message: `Quantity maximum value is 10`
      }
    }

    let orderId = await Order.find({ userId: data.userId })


    let getIsPaidToFalse = orderId.map(element => {
      if (element.isPaid == false) {
        return element.id
      }
    })


    let orderData = await Order.findById(getIsPaidToFalse).then(myOrder => {

      if (!myOrder) {
        return false
      }

      if (myOrder.isPaid == true) {
        return false
      }

      myOrder.products.push({
        productId: getProduct._id,
        productBrand: getProduct.brand,
        productCategories: getProduct.categories,
        productName: getProduct.name,
        productScreenSize: getProduct.screenSize,
        productCPU: getProduct.cpu,
        productVideoCard: getProduct.videoCard,
        productRam: getProduct.ram,
        productStorage: getProduct.storage,
        productOS: getProduct.OS,
        productPrice: getProduct.price,
        productSource: getProduct.source,
        quantity: data.quantity,
      })
      let numberArray = []

      myOrder.products.map(product => {
        const totalAmount = product.productPrice * product.quantity
        return numberArray.push(totalAmount)
      })

      const sum = numberArray.reduce((partialSum, a) => partialSum + a, 0)

      return myOrder.save().then((user, error) => {
        if (error) {
          return false
        } else {
          console.log(sum)
          console.log(getIsPaidToFalse + " my data ")
          return Order.findByIdAndUpdate(getIsPaidToFalse,
            { totalAmount: sum }).then((user, error) => {
              if (error) {
                return false
              } else {
                return true
              }
            })
        }
      })
    })




    if (orderData == false || orderId.length == 0) {
      await newOrder.save();
      return {
        message: `Order successly placed`,
      }
    }

    if (orderData == true) {
      return {
        message: `Order successly placed`,
      }
    }
  }
}


// Check Specific Order

const userSpecificOrder = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {

    const result = await Order.findById(data.orderId)

    if (!result) {
      return {
        message: 'Nothing Found!'
      }

    }

    if (result.length == []) {
      return 'Nothing Found!'
    }

    result.userId = undefined;
    result.products.forEach(element => {
      element.productId = undefined

    })

    return result
  }
}


// Check User Order
const allOrder = async (data) => {

  console.log(data)
  if (data.isAdmin) {
    let result = await Order.find({})

    if (!result) {
      return 'Nothing Found!'
    }

    if (result.length == []) {
      return 'Nothing Found!'
    }



    result.forEach(element => {
      element.userId = undefined;
      element.products.forEach(item => {
        item.productId = undefined;
      })
    })

    return result

  } else {

    return {
      message: 'User is not allowed to access in this page'
    }

  }

}



// Edit User Order
const editUserOrder = async (data) => {
  if (data.isAdmin) {

    return {
      message: 'Admin is not allowed to access in this page'
    }

  } else {

    /*     const result = await Order.findById(data.orderId)
    
        console.log(" hanapin ang data result " + data.quantity + " :: ::  " + result.isPaid)
    
    
        if (result.isPaid == true) {
          return {
            message: `Sorry this product is already paid !`
          }
        }
    
        if (!result) {
    
          return {
            message: `No product found!`
          }
        }
    
        if (!Number.isInteger(data.quantity)) {
          return {
            message: `Quantity must be a number!`
          }
        } 
        */

    if (data.quantity <= 0) {
      return {
        message: `Quantity minimun value is 1`
      }
    }

    if (data.quantity > 10) {
      return {
        message: `Quantity maximum value is 10`
      }
    }

    console.log("data :: product id" + data.productId + " ::  :: dataQuantity :: " + data.quantity)
    const cartResult = await Order.findById(data.orderId).then(myOrder => {
      console.log(" my order status data :: " + myOrder)
      myOrder.products.filter(product => {

        console.log(product.id.toString() + "myProduct")
        if (product._id == data.productId) {

          product.quantity = data.quantity
          return product;
        }
      })

      let numberArray = []

      myOrder.products.map(product => {
        const totalAmount = product.productPrice * product.quantity
        return numberArray.push(totalAmount)
      })

      const sum = numberArray.reduce((partialSum, a) => partialSum + a, 0)
      console.log(sum)
      return myOrder.save().then((user, error) => {
        if (error) {
          return false
        } else {

          return Order.findByIdAndUpdate(data.orderId, {
            totalAmount: sum
          }).then((user, error) => {
            if (error) {
              return false
            } else {
              console.log(sum)
              console.log('working')
              return true
            }
          })
        }
      })
    });


    if (cartResult == true) {
      console.log(' it is true working')
      return {
        message: `Product successfully updated to your order`
      }
    }
    if (cartResult == false) {
      console.log(" it is false")
      return {
        message: `Product failed updated to your order`
      }
    }


  }
}


// User delete order

const userDeleteOrder = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {



    const result = await Order.findById(data.orderId)

    if (!result) {
      return {
        message: `No product found!`
      }
    }



    if (result.products.length == 0) {

      const deleteWholeOrder = await Order.findByIdAndDelete(data.orderId)

      if (!deleteWholeOrder) {

        return "error whole data failed"
      }

      return `successful delete whole Order`
    }




    const deleteUserOrder = await Order.findById(result._id).then(myOrder => {

      console.log(myOrder.isPaid + "check this")
      if (myOrder.isPaid == true) {
        return false
      }

      const myData = myOrder.products.findIndex(item => item.id === data.productId)

      if (myData == -1) {
        return {
          message: 'No result found'
        }
      }

      myOrder.products.splice(myData, 1)
      console.log(myData)

      let numberArray = []

      myOrder.products.map(product => {
        const totalAmount = product.productPrice * product.quantity
        return numberArray.push(totalAmount)
      })



      const sum = numberArray.reduce((partialSum, a) => partialSum + a, 0)

      return myOrder.save().then((user, error) => {
        if (error) {
          return false
        } else {
          return Order.findByIdAndUpdate(data.orderId, {
            totalAmount: sum
          }).then((user, error) => {
            if (error) {
              return false
            } else {
              return true
            }
          })
        }
      })
    })


    if (deleteUserOrder == true) {
      return true
    } else {
      return false
    }

  }
}


/* Delete all order */

const deleteOrder = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    const result = await Order.findByIdAndDelete(data.orderId)

    if (!result) {
      return false
    }

    return true
  }
}


const payOrder = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {
    console.log(data + "galing sa react js")
    console.log(data._id + "galing sa react js")


    const creditCard = await Paypal.findOne({ creditCard: data.creditCard })
    const expire_date = await Paypal.findOne({ cardExpDate: data.cardExpDate })
    const security_code = await Paypal.findOne({ securityCode: data.securityCode })
    const payPal_id = await Paypal.findOne({ cardNumber: data.cardNumber })
    const orderResult = await Order.findById(data.orderId)

    if (!orderResult) {
      return `Nothing Found`
    }

    if (!creditCard) {
      return 'Credit Card type not found'
    }

    if (!payPal_id) {
      return 'Card number not found'
    }

    if (!security_code) {
      return 'Security code not found'
    }

    if (!expire_date) {
      return 'Expiry date not found'
    }





    const isUserCardUpdated = await Paypal.findById(payPal_id.id).then(data => {

      if (orderResult.totalAmount > data.amount) {
        return false
      }


      const products = data.orders.push(orderResult)



      console.log(products)

      const subtract = data.amount - orderResult.totalAmount

      data.amount = subtract;

      return data.save().then((user, error) => {
        if (error) {
          return false
        }
        return true
      });
    })

    console.log(isUserCardUpdated)

    const isUserOrderUpdated = await Order.findById(orderResult).then(orderData => {
      if (orderData.isPaid == true) {
        return false
      }

      console.log(orderData + 'myData Order')

      orderData.status = 'paid'
      orderData.isPaid = true

      return orderData.save().then((user, error) => {
        if (error) {
          return false
        }
        return true
      });
    })

    console.log(isUserOrderUpdated)

    orderResult.userId = undefined
    orderResult.id = undefined

    orderResult.products.forEach(element => {
      element.id = undefined
      element.productId = undefined

    })
    console.log("the result is " + orderResult)



    if (isUserCardUpdated && isUserOrderUpdated) {
      return {
        message: `Thank you ${orderResult.firstName} ${orderResult.lastName} `,
        product: orderResult.products,
        TotalAmount: ` ${orderResult.totalAmount}`,

      }

    } else {
      return false;
    }

  }
}

module.exports = {
  createOrder,
  allOrder,
  editUserOrder,
  userDeleteOrder,
  payOrder,
  userSpecificOrder,
  deleteOrder
}