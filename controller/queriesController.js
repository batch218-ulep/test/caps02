const User = require('../models/User.js')
const Queries = require('../models/Order.js')


const userQuery = async (data, reqBody) => {

  if (data.isAdmin) {
    return {
      message: "Admin is now allow to access this page"
    }
  } else {
    let user = await User.findById(data.userId)

    let newUser = new User({
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      mobileNo: user.mobileNo,
      address: user.address,
      supportType:
    })

    const userRegister = await newUser.save()

    if (!userRegister) {
      return "Registration Failed"
    }

    return {
      message: ` Congratulations, ${firstName} ${lastName}  your account has been successfully created.`,
      firstName: firstName,
      lastName: lastName,
      successRegister: true
    }

  }
}

}


module.exports = {
  userQuery,
}