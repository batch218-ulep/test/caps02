const bcrypt = require('bcrypt')
const User = require('../models/User.js')
const Order = require('../models/Order.js')
const auth = require('../auth.js')
const validator = require('validator')

const userRegister = async (reqBody) => {
  const { firstName, lastName, email, mobileNo, address, password } = reqBody

  const result = await User.find({ email: email })

  const mobileNumberUnuqie = await User.find({ mobileNo: mobileNo })

  const fields = Object.keys(reqBody)
  // const fieldEntry = Object.entries(reqBody)
  const allowedProperty = ['firstName', 'lastName', 'email', 'mobileNo', 'address', 'password']
  // Check if the reqBody Fieldname is correct if not return invalid Field names
  const checkFieldValidators = fields.every(update => allowedProperty.includes(update))
  // Check the reqBody missing object property
  const missingFields = allowedProperty.filter(item => fields.indexOf(item) == -1)
  // Convert the reqBody Object to array that the value is object
  // const data =  fieldEntry.map(([fieldName, fieldValue]) => ({ [fieldName] : fieldValue}))  

  const lowerCase = new RegExp('(?=.*[a-z])');
  const upperCase = new RegExp('(?=.*[A-Z])');
  const number = new RegExp('(?=.*[0-9])');
  const special = new RegExp('(?=.*[!@#\$%\^&\*])');
  const eightChar = new RegExp('(?=.{8,})');

  if (!eightChar.test(password)) {
    return {
      message: "The password length must be greater than or equal to 8",
      passwordLength: false
    }
  }

  if (!lowerCase.test(password)) {
    return {
      message: "The password must contain one or more lowercase characters",
      noLowerCase: false
    }
  }

  if (!upperCase.test(password)) {
    return {
      message: "The password must contain one or more uppercase characters",
      noUpperCase: false
    }
  }

  if (!number.test(password)) {
    return {
      message: "The password must contain one or more numeric values",
      noNumber: false
    }
  }

  if (!special.test(password)) {
    return {
      message: "The password must contain one or more special characters",
      noSpecialCharater: false
    }
  }

  if (mobileNumberUnuqie.length >= 1) {
    return {
      message: "The phone number is already taken",
      duplicatePhoneNumber: false
    }
  }

  if (fields.length < allowedProperty.length) {
    return `${missingFields.toString()} Field is required!`
  }

  if (!validator.isEmail(email)) {
    return {
      message: 'Email is invalid',
      invalidEmail: false
    }
  }

  if (!checkFieldValidators) {
    return {
      message: `Field name is invalid!`
    }
  }

  if (mobileNo.length != 11) {
    return {
      message: `mobile number length is invalid`,
      maxPhoneLength: false
    }
  }

  if (result.length > 0) {
    return {
      message: "The email is already taken",
      emailIsTaken: false
    }
  }

  if (firstName.length == 0 || lastName.length == 0 || email.length == 0 || mobileNo.length == 0 || address.length == 0 || password.length == 0) {

    return {
      message: "Please complete the registration form"
    }
  }



  let newUser = new User({
    firstName: firstName,
    lastName: lastName,
    email: email,
    mobileNo: mobileNo,
    address: address,
    password: bcrypt.hashSync(password, 10),
  })

  const userRegister = await newUser.save()

  if (!userRegister) {
    return "Registration Failed"
  }

  return {
    message: ` Congratulations, ${firstName} ${lastName}  your account has been successfully created.`,
    firstName: firstName,
    lastName: lastName,
    successRegister: true
  }

}

// Validate if user is reggitered or not
const userLogin = async (reqBody) => {
  const { email, password } = reqBody
  const result = await User.findOne({ email: email })

  if (result == null) {
    return {
      message: 'Your username and password is not match',
      login: false
    }
  } else {
    const isPasswordCorrect = bcrypt.compareSync(password, result.password);

    if (isPasswordCorrect) {
      return {
        access: auth.createAccessToken(result),
        login: true,
        firstName: result.firstName
      }
    }

    return {
      message: 'Your username and password is not match',
      login: false
    }
  }

}

// Read user profile
const userProfile = async (data) => {
  console.log(data)
  if (data.isAdmin == true || data.isAdmin == false) {
    const result = await User.findById(data.userId)

    if (!result) {
      return `The user id is failed`
    }

    result.password = undefined

    return {
      message: 'User found',
      user: result
    }
  } else {
    return {
      message: 'User is not allowed to access in this page'
    }
  }
}

// Check User Order
const userOrder = async (data) => {
  if (data.isAdmin) {
    return {
      message: 'Admin is not allowed to access in this page'
    }
  } else {

    let result = await Order.find({ userId: data.userId })

    if (!result) {
      return 'Nothing Found'
    }

    if (result.length == []) {
      return 'Nothing Found'
    }

    result.forEach(element => {
      element.userId = undefined;
      element.products.forEach(item => {
        item.productId = undefined;
      })
    })

    return result

  }

}


// checkSpecificProfile

const checkSpecificProfile = async (data, paramsId) => {
  console.log(data)
  if (data.isAdmin == true) {
    const result = await User.findById(paramsId)

    if (!result) {
      return `The user id is failed`
    }


    result.password = undefined


    return result
  } else {
    return {
      message: 'User is not allowed to access in this page'
    }
  }
}


// Read user profile
const readAllUser = async (data) => {
  console.log(data)
  if (data.isAdmin == true) {
    const result = await User.find({})

    if (!result) {
      return `The user id is failed`
    }


    result.forEach(element => {
      element.password = undefined

    })

    return {
      message: 'All registered users',
      userAll: result
    }
  } else {
    return {
      message: 'User is not allowed to access in this page'
    }
  }
}

// Update user profile
const updateUserProfile = async (data) => {
  console.log(data.reqBody)
  if (data.isAdmin == true || data.isAdmin == false) {
    const result = await User.findByIdAndUpdate(data.userId, {
      source: data.reqBody.source,
      firstName: data.reqBody.firstName,
      lastName: data.reqBody.lastName,
      email: data.reqBody.email,
      mobileNo: data.reqBody.mobileNo,
      address: data.reqBody.address,
    })

    if (!result) {
      return `The user update is failed`
    }

    console.log("ito ang result ay " + result)

    result.password = undefined

    return {
      message: 'update successful',
      user: result
    }
  } else {
    return {
      message: 'User is not allowed to access in this page'
    }
  }
}

// Update user profile
const updateUserPassword = async (data) => {

  if (data.isAdmin == true || data.isAdmin == false) {

    const { password } = data.reqBody

    const fields = Object.keys(data.reqBody)
    console.log(fields)
    // const fieldEntry = Object.entries(reqBody)
    const allowedProperty = ['password']
    // Check if the reqBody Fieldname is correct if not return invalid Field names
    const checkFieldValidators = fields.every(update => allowedProperty.includes(update))
    // Check the reqBody missing object property
    const missingFields0 = allowedProperty.filter(item => fields.indexOf(item) == -1)



    const lowerCase = new RegExp('(?=.*[a-z])');
    const upperCase = new RegExp('(?=.*[A-Z])');
    const number = new RegExp('(?=.*[0-9])');
    const special = new RegExp('(?=.*[!@#\$%\^&\*])');
    const eightChar = new RegExp('(?=.{8,})');

    if (!eightChar.test(password)) {
      return {
        message: "The password length must be greater than or equal to 8",
        passwordLength: false
      }
    }

    if (!lowerCase.test(password)) {
      return {
        message: "The password must contain one or more lowercase characters",
        noLowerCase: false
      }
    }

    if (!upperCase.test(password)) {
      return {
        message: "The password must contain one or more uppercase characters",
        noUpperCase: false
      }
    }

    if (!number.test(password)) {
      return {
        message: "The password must contain one or more numeric values",
        noNumber: false
      }
    }

    if (!special.test(password)) {
      return {
        message: "The password must contain one or more special characters",
        noSpecialCharater: false
      }
    }

    if (!checkFieldValidators) {
      return {
        message: `Field name is invalid!`
      }
    }


    if (fields.length < allowedProperty.length) {
      return `${missingFields.toString()} Field is required!`
    }

    if (password.length == 0) {

      return {
        message: "Please complete the registration form"
      }
    }



    const result = await User.findByIdAndUpdate(data.userId, {
      password: bcrypt.hashSync(password, 10)
    })

    result.password = undefined

    if (!result) {
      return `The user password update is failed`
    }

    return true
  } else {
    return `Please Authenticate`
  }
}


const editUserData = async (data, userId) => {
  if (data.isAdmin) {

    const checkEmail = await User.find({ email: data.reqBody.email, })

    if (checkEmail.length > 1) {
      return {
        message: 'Email is already exist'
      }
    }

    const result = await User.findByIdAndUpdate(userId, {
      firstName: data.reqBody.firstName,
      lastName: data.reqBody.lastName,
      email: data.reqBody.email,
      mobileNo: data.reqBody.mobileNo,
      address: data.reqBody.address,
      source: data.reqBody.source
    })

    if (!result) {
      return `The user updated is failed`
    }

    result.password = undefined

    return {
      message: 'update successfully',
      user: result
    }
  } else {
    return {
      message: 'User is not allowed to access in this page'
    }
  }
}

const setUserToAdmin = async (data, userId) => {
  if (data.isAdmin) {
    const result = await User.findByIdAndUpdate(userId, {
      isAdmin: true
    })

    if (!result) {
      return ` failed to set the user into admin `
    }

    result.password = undefined

    return {
      message: 'update successfully',
      user: result
    }
  } else {
    return {
      message: 'User is not allowed to access in this page'
    }
  }
}


// Archive User Status

const archiveUser = async (data, userId) => {
  console.log(data + " archive data")
  if (data.isAdmin) {
    const result = await User.findByIdAndUpdate(userId, {
      isAdmin: false
    })

    if (!result) {
      return ` failed to set the user into admin `
    }

    result.password = undefined

    return {
      message: 'update successfully',
      user: result
    }
  } else {
    return {
      message: 'User is not allowed to access in this page'
    }
  }
}


const requestPassword = async (reqBody) => {
  const { email, mobileNo, newPassword } = reqBody

  const emailResult = await User.findOne({ email: email })

  const phoneResult = await User.findOne({ mobileNo: mobileNo })

  if (!emailResult) {
    return 'Nothing Found'
  }

  const fields = Object.keys(reqBody)

  console.log(fields)
  // const fieldEntry = Object.entries(reqBody)
  const allowedProperty = ['email', 'mobileNo', 'newPassword']
  // Check if the reqBody Fieldname is correct if not return invalid Field names
  const checkFieldValidators = fields.every(update => allowedProperty.includes(update))
  // Check the reqBody missing object property

  if (!checkFieldValidators) {
    return {
      message: `Field name is invalid!`
    }
  }

  const lowerCase = new RegExp('(?=.*[a-z])');
  const upperCase = new RegExp('(?=.*[A-Z])');
  const number = new RegExp('(?=.*[0-9])');
  const special = new RegExp('(?=.*[!@#\$%\^&\*])');
  const eightChar = new RegExp('(?=.{8,})');

  if (!eightChar.test(newPassword)) {
    return {
      message: "The password length must be greater than or equal to 8"
    }
  }

  if (!lowerCase.test(newPassword)) {
    return {
      message: "The password must contain one or more lowercase characters"
    }
  }

  if (!upperCase.test(newPassword)) {
    return {
      message: "The password must contain one or more uppercase characters"
    }
  }

  if (!number.test(newPassword)) {
    return {
      message: "The password must contain one or more numeric values"
    }
  }

  if (!special.test(newPassword)) {
    return {
      message: "The password must contain one or more special characters"
    }
  }

  let emailResultData
  let phoneResultData



  if (emailResult.email == email) {

    emailResultData = await User.findByIdAndUpdate(emailResult.id, {
      password: bcrypt.hashSync(newPassword, 10)
    })

    console.log(emailResultData)

    if (!emailResultData) {
      return false
    }

    return {
      message: `Your reset password is successful`
    }

  }

  if (phoneResult.mobileNo == mobileNo) {

    phoneResultData = await User.findByIdAndUpdate(phoneResult.id, {
      password: bcrypt.hashSync(newPassword, 10)
    })

    console.log(phoneResultData)

    if (!phoneResultData) {
      return false
    }

    return {
      message: `Your reset password is successful`
    }

  }





  return {
    message: 'Your username and phone number is not authenticated'
  }




}


module.exports = {
  userRegister,
  userLogin,
  userProfile,
  userOrder,
  readAllUser,
  updateUserProfile,
  updateUserPassword,
  editUserData,
  setUserToAdmin,
  archiveUser,
  requestPassword,
  checkSpecificProfile
}