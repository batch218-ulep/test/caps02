const jwt = require('jsonwebtoken')

const secret = "EcommerceWebsiteAPI"

// To create a token using the json webtoken package from NPM
const createAccessToken = (user) => {
    const data = {
       id: user._id,
       email: user.email,
       isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {})
}

// To verify a token from the request/from postman
const verify = (req, res, next) => {
    let token = req.headers.authorization
    //  console.log("checking user token " + token );
     if(token == undefined) {
        res.send ("No token , Please put your token")
     }

    if(typeof token !== 'undefined') {
  
      token = token.slice(7, token.length)

      // To veryfy the token using jwt, it requires the actual token and the secret key that was used to create it
      return jwt.verify(token, secret, (error, data) => {
          if(error) {
            return res.send( { auth: 'Failed.'} )
          } else {
             next()
          }
      })

    } else {
      return null
    }
}


const decode = (token) => {
  if(typeof token !== 'undefined'){
     token = token.slice(7, token.length)

     return jwt.verify(token, secret, (error, data) => {
        if(error) {
          return null
        } else {
          return  jwt.decode(token, {complete: true}).payload
        }
     })
  } else {
     return null 
  }
}


module.exports = {
  createAccessToken,
  verify,
  decode,
}